package com.control.back.halo.manage.service;

import com.control.back.halo.basic.service.IBaseService;
import com.control.back.halo.manage.entity.Admin;

public interface IAdminService extends IBaseService<Admin, Long> {

    Admin findByUsername(String username);

    void saveOrUpdate(Admin admin);

    Boolean saveAdminRoles(Long id, Long[] roles);

}
