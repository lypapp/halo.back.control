package com.control.back.halo.manage.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.basic.utils.UserUtils;
import com.control.back.halo.manage.service.IFunctionService;

@Controller
public class IndexController extends BaseController {

    @Autowired
    private IFunctionService functionService;

    @RequestMapping({ "", "index" })
    public String index(Model mv) {
        mv.addAttribute("functions", functionService.loadFunctionByAdminId(UserUtils.getCurrentUser().getId()));
        return "index";
    }

    /**
     * 退出
     */
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout() {
        SecurityUtils.getSubject().logout();
        return "redirect:login.html";
    }

    /**
     * 注册
     * 
     * @return
     */
    @RequestMapping("/register")
    public String registerView() {
        return "/authorization/register";
    }

}
